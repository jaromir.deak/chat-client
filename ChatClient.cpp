// ChatClient.cpp : Defines the entry point for the console application.
//

#define WIN32_LEAN_AND_MEAN

#include "stdafx.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <vector>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define BUF_SIZE 255
#define DEFAULT_PORT "27015"

using namespace std;

string ExtractString(char* recvbuf, int size);
DWORD WINAPI Receive(LPVOID lpParam);
SOCKET OpenSocket(char* adress);
int ReadAndSend(const SOCKET &socket);

int main(int argc, char **argv)
{
	SOCKET socket;

	DWORD   threadId;
	HANDLE  threadHandle;

	// Validate the parameters
	if (argc != 2) {
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}

	// Init network connection
	socket = OpenSocket(argv[1]);

	// Start receiving thread
	threadHandle = CreateThread(NULL, 0, Receive, &socket, 0, &threadId);

	// Serve input thread
	return ReadAndSend(socket);

	return 0;
}

string ExtractString(char* recvbuf, int size)
{
	// Allocate memory of temporary char array
	char *substr = (char*)malloc(sizeof(char)*(size + 1));
	// Copy the particular part of buffer to temporary char array
	strncpy_s(substr, sizeof(char)*(size + 1), recvbuf, sizeof(char)*size);
	substr[size] = '\0';
	// Create string representation
	string str = string(substr);
	// Free used memory
	free(substr);
	// Return the string representation
	return string(str);
}

DWORD WINAPI Receive(LPVOID lpParam)
{
	HANDLE stdoutHandle;
	SOCKET* socket;
	char buffer[BUF_SIZE];

	// Make sure there is a console to receive output results. 
	stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (stdoutHandle == INVALID_HANDLE_VALUE)
		return 1;

	// Cast the parameter to the correct data type.
	socket = (SOCKET*)lpParam;

	int result;
	string message;
	while (true) {
		// Empty message buffer
		message = "";

		// Receive first part of the message
		result = recv(*socket, buffer, BUF_SIZE, 0);

		// Read till incoming buffer is still full
		u_long waitingBytes = 0;
		ioctlsocket(*socket, FIONREAD, &waitingBytes);
		while (waitingBytes > 0) {
			message.append(ExtractString(buffer, result));
			result = recv(*socket, buffer, BUF_SIZE, 0);
			ioctlsocket(*socket, FIONREAD, &waitingBytes);
		}

		if (result > 0) {
			message.append(ExtractString(buffer, result));
		}

		cout << "New message: " << message << endl;
		if (result == 0)
			cout << "Connection closing..." << endl;

		if (result < 0) {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(*socket);
			WSACleanup();
			return 1;
		}

		// Sleep
		Sleep(1000);
	}

	return 0;
}

SOCKET OpenSocket(char* adress) {
	WSADATA wsaData;
	SOCKET connectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return NULL;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(adress, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return NULL;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		connectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (connectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return NULL;
		}

		// Connect to server.
		iResult = connect(connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(connectSocket);
			connectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (connectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return NULL;
	}

	return connectSocket;
}

int ReadAndSend(const SOCKET &socket)
{
	string message = "";
	int result;
	while (true) {
		// Read from input
		getline(cin, message);

		// Write to socket
		result = send(socket, message.c_str(), (int)strlen(message.c_str()), 0);
		if (result == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(socket);
			WSACleanup();
			return 1;
		}
	}
}